
[![pipeline status](https://gitlab.com/bryanmay/hello-world/badges/master/pipeline.svg)](https://gitlab.com/bryanmay/hello-world/commits/master) [![coverage report](https://gitlab.com/bryanmay/hello-world/badges/master/coverage.svg)](https://gitlab.com/bryanmay/hello-world/commits/master)

# Hello World
- This is a project to experiment with building gitlab pages documentation using Sphinx!

# Requirements (MacOS)
 - `brew install python3`
 - `pip3 install Sphinx`
 - `pip3 install sphinx-rtd-theme`

# Build
 - `git clone git@gitlab.com:bryanmay/hello-world.git`
 - `cd hello-world`
 - `make html`

# Deployment
 - This will deploy to http://bryanmay.gitlab.io/hello-world
 - Every time a change is made, the `pages` ci job is run to redeploy the new changes.

# Test
 - On push/merge, a `test` job will check all url links on http://bryanmay.gitlab.io/hello-world. If all come back with 2xx, then the job passes.

# How to update content
 - `cd hello-world/source`
 - `nano <index.rst> or <firstpage.md>`. I've been using [Atom](https://atom.io/) with the [markdown preview feature](https://atom.io/packages/markdown-preview)
